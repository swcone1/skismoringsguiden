package one.swc.skismringsguiden

import android.app.PendingIntent
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.io.IOException
import java.net.URL

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        Log.d(TAG, "From: " + remoteMessage!!.from!!)
        Log.d(TAG, "data" + remoteMessage.data)
        //Log.d(TAG, "messeage type " + remoteMessage?.messageType!!)
        val title: String
        val message: String = remoteMessage.data["message"].toString()
        val img_url: String = remoteMessage.data["img_url"].toString()

        title = remoteMessage.data["title"].toString()


        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT
        )
        val kanalid = getString(R.string.default_notification_channel_id)
        Log.d(TAG, "default_notification_channel_id $kanalid")
        //load img
        var bmp: Bitmap? = null
        try {
            val `in` = URL(img_url).openStream()
            bmp = BitmapFactory.decodeStream(`in`)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val mBuilder = NotificationCompat.Builder(this, kanalid)
            .setSmallIcon(R.drawable.ic_stat_ac_unit)
            .setContentTitle(title)
            .setContentText(message).setStyle(
                NotificationCompat.BigPictureStyle()
                    .bigPicture(bmp)
            )
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)


        val notificationManager = NotificationManagerCompat.from(this)
        notificationManager.notify(0, mBuilder.build())
        Log.d(TAG, "Notification builded ")

    }

    override fun onNewToken(token: String?) {
        Log.d(TAG, "Refreshed token: " + token!!)
        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
    }
}