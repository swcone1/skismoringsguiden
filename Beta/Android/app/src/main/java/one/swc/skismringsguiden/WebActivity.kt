package one.swc.skismringsguiden


import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.InterstitialAd
import com.google.firebase.analytics.FirebaseAnalytics


class WebActivity : AppCompatActivity() {
    private var webView: WebView? = null
    private var textViewAbout: TextView? = null
    private var ButtonViewAbout: Button? = null
    private var mFirebaseAnalytics: FirebaseAnalytics? = null
    private var mAdView: AdView? = null
    private lateinit var mInterstitialAd: InterstitialAd



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        setContentView(R.layout.activity_web)
        ButtonViewAbout = findViewById<View>(R.id.buttonviewabout) as Button
        textViewAbout = findViewById<View>(R.id.textviewabout) as TextView
        webView = findViewById<View>(R.id.WebView1) as WebView

        //Advview
        MobileAds.initialize(this, "ca-app-pub-2106018268188752~9296363419")
        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = "ca-app-pub-2106018268188752/8506041171"
//test adss
        //mInterstitialAd.adUnitId = "ca-app-pub-3940256099942544/1033173712"


        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mAdView = findViewById(R.id.adView)
        val adRequest =
            AdRequest.Builder().addTestDevice("AD9AF5A795246BB2C7D73DCBB49BBCBC").build()
        mAdView!!.loadAd(adRequest)


        //WebView Settings
        webView!!.settings.javaScriptEnabled = true
        webView!!.settings.builtInZoomControls = false
        webView!!.webViewClient = WebViewClient()
        webView!!.settings.loadWithOverviewMode = true
        webView!!.settings.useWideViewPort = true
        webView!!.settings.cacheMode = WebSettings.LOAD_NO_CACHE


        //Webview load url
        webView!!.loadUrl("http://swc.one/skismoring/index.html")
        Toast.makeText(applicationContext, "Velkommen!", Toast.LENGTH_SHORT).show()
    }


    fun exitAbout(view: View) {
        webView!!.visibility = View.VISIBLE
        textViewAbout!!.visibility = View.INVISIBLE
        ButtonViewAbout!!.visibility = View.INVISIBLE
        //webView.loadUrl("http://swc.one/skismoring/index.html");
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_web, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        if (id == R.id.action_settings) {
            val intent = Intent(this, MySettingsActivity::class.java)
            startActivity(intent)
        }

        if (id == R.id.action_web) {
            webView!!.loadUrl("http://swc.one/skismoring/index.html")
            Toast.makeText(applicationContext, "Oppdaterer", Toast.LENGTH_SHORT).show()
        }

        if (id == R.id.action_about) {
            //webView.getVisibility();
            webView!!.visibility = View.INVISIBLE
            textViewAbout!!.visibility = View.VISIBLE
            ButtonViewAbout!!.visibility = View.VISIBLE
        }

        if (id == R.id.action_exit) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (webView!!.canGoBack()) {
                        webView!!.goBack()
                        mInterstitialAd.loadAd(AdRequest.Builder().build())
                        mInterstitialAd.show()

                    } else {
                        finish()
                    }
                    return true
                }
            }

        }
        return super.onKeyDown(keyCode, event)
    }
}