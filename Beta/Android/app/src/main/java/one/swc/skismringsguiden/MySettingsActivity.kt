package one.swc.skismringsguiden

import android.app.Activity
import android.os.Bundle
import android.preference.PreferenceFragment

class MySettingsActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Display the fragment as the main content.
        val mFragmentManager = fragmentManager
        val mFragmentTransaction = mFragmentManager
            .beginTransaction()
        val mPrefsFragment = PrefsFragment()
        mFragmentTransaction.replace(android.R.id.content, mPrefsFragment)
        mFragmentTransaction.commit()
    }

    class PrefsFragment : PreferenceFragment() {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences)

        }
    }

}
