package one.swc.skismringsguiden

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.Menu
import android.view.MenuItem
import android.widget.ProgressBar
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging

class MainActivity : AppCompatActivity() {
    private var mFirebaseAnalytics: FirebaseAnalytics? = null
    internal var progress = 0
    internal lateinit var simpleProgressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        setContentView(R.layout.activity_main)
        simpleProgressBar = findViewById<View>(R.id.progressBar) as ProgressBar
        setProgressValue(progress)
        createNotificationChannel()
        FirebaseMessaging.getInstance().subscribeToTopic("Oppdateringer")
        FirebaseMessaging.getInstance().subscribeToTopic("test")
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val token = task.result!!.token

                // Log and toas
                Log.d(TAG, token)
            })
        createNotificationChannel()
        createNotificationChannel2()
    }

    private fun startWebVeiw() {
        val intent = Intent(this, WebActivity::class.java)
        startActivity(intent)
    }

    private fun setProgressValue(progress: Int) {

        // set the progress
        simpleProgressBar.progress = progress
        // thread is used to change the progress value
        val thread = Thread(Runnable {
            try {
                Thread.sleep(30)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

            setProgressValue(progress + 2)
        })
        thread.start()
        if (progress == 100) {
            startWebVeiw()
            finish()
        }
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)

    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val description = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("channel", name, importance)
            channel.description = description
            channel.enableLights(true)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager =
                getSystemService<NotificationManager>(NotificationManager::class.java)
            notificationManager!!.createNotificationChannel(channel)
        }

    }

    private fun createNotificationChannel2() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name2)
            val description = getString(R.string.channel_description2)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel2 = NotificationChannel("test", name, importance)
            channel2.description = description
            channel2.enableLights(true)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager =
                getSystemService<NotificationManager>(NotificationManager::class.java)
            notificationManager!!.createNotificationChannel(channel2)
        }
    }

    companion object {

        private const val TAG = "pushmeldingstjenesten"
    }
}